var tests = require('./tests.js');

init();

function init(){
  console.log('Starting up');

  tests.forEach(function(test, index){
    console.log('Test ', index, ' is running');
    // console.log(test);
    printTest(test.map);
    var result = isSolved(test);
    console.log(result === test.isSolved ? "Pass" : "Fail");
    console.log("\n******** \n");
  });
}

function printTest(test){
  test.forEach(function(row){
    console.log(row);
  });
}

function isSolved(test){
  return false;
}
