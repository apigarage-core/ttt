module.exports  = [
  {
    map: [
      [1, 1, 1],
      [1, 0, 0],
      [0, 1, 0]
    ],
    isSolved: true
  },
  {
    map: [
      [1, 0, 0],
      [1, 1, 1],
      [0, 1, 0]
    ],
    isSolved: true
  },
  {
    map: [
      [1, 0, 0],
      [0, 0, 1],
      [1, 1, 1]
    ],
    isSolved: true
  },
  {
    map: [
      [0, 1, 0],
      [1, 0, 0],
      [0, 1, 1]
    ],
    isSolved: true
  },
  {
    map: [
      [0, 1, 1],
      [1, 0, 0],
      [0, 1, 0]
    ],
    isSolved: true
  },
  {
    map: [
      [0, 1, 1],
      [1, 1, 0],
      [0, 1, 0]
    ],
    isSolved: true
  },
  {
    map: [
      [0, 1, 1],
      [1, 1, 0],
      [0, 0, 1]
    ],
    isSolved: false
  }
];
